

package security.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class AppController {

        @GetMapping("/admin")
        public String viewAdminPage() {
            return "admin";
        }
        
        @GetMapping("/user")
        public String viewUserPage() {
            return "user";
        }
        
        @GetMapping("/")
        public String viewIndexPage() {
            return "index";
        }
}
